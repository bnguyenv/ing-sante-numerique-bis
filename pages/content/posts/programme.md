---
title: Programme
slug: programme
description: null
draft: false
menu:
  main:
    name: Programme
    weight: 999
url: /programme/
---

# 1er rencontre à Rennes (2022)

(*ce programme est susceptible aux changements pendant les prochains jours*)

*Dates :* mercredi 7 et vendredi 8 décembre

*Lieu :* Inria de l'université de Rennes

## Jour 1

- 10h00-10h30: Accueil
- 10h30-11h00: Tour de table
- 11h00-12h00: Activités *Santé Numérique (SN)* par centre.
    - Lyon (Antoine Fraboulet).
    - Rennes (Eric Poiseau)
    - Bordeaux (Dan Dutartre)
    - Saclay (Benjamin Nguyen-Van-Yen)
    - Paris (Mauricio Diaz)
- 12h00-13h30: Déjeuner
- 13h30-15h30: Présentations de projets, partie 1.
    - Primetime IRE (Luc Laffite)
    - RadioNLP (Salim Sadoune)
    - OpenViBE (Thomas Prampart)
    - Shanoir (Michael Kain)
    - HappyFeat, an interactive and efficient BCI framework for clinical applications (Arthur Desbois)
    - Fed-BioMed- Deploying Federated Learning in Real-World Health Applications (Yannick Bouillard, Francesco Cremonesi)
    - Apprentissage Fédéré : logiciel declearn + projets en partenariat avec les CHU de Lille, Amiens, Caen et Rouen" (Paul Andrey)
    - AIstroSight et HyperFlow (Jan-Michael Rye)
- 15h30-15h45: Pause café.
- 15h45-17h45: Présentations de projets, partie 2.
    - Clinica (Nicolas Gensollen)
    - ClinicaDL (Camille Brianceau)
    - MedInria (Julien Castelneau)
    - BioImageIT: Integration of data-management with analysis (Leo Maury)
    - MedKit (Olivier Birot)
    - URGE "Analyse des parcours patients aux urgences et optimisation des prises en charge" (Benjamin Nguyen-Van-Yen)
    - Gnomon (Tristan Cabel)
    - Vidjil (Florian Thonier)

## Jour 2

- 08h00-08h15: Accueil
- 08h15-10h15: Session de travail 1, **Gestion de données**
    - Formats: présentations introductoires DICOM (Eric Poiseau), BIDS (Ghislain Vaillant).
    - Accès: PACS, Shanoir
    - Plateformes de données: SNDS, HDH, PDS-Inria (Kim-Tâm Huynh, Mauricio Diaz, Dan Dutartre, rSEDs).
    - Systèmes de contôle de version pour les données.
    - Anonimisation, pseudonimisation
- 10h15-10h30: Pause café
- 10h30-12h30: Session de travail 2, **Chaine de traitement pour le dev. logiciel**
    - Comment les données sont intégrées à la chaîne de traitement (cas spécial pour le traitement féderé) ?
    - Outils pour la chaîne de traitement: Nypipe/Pydra (Ghislain Vaillant).
    - Déploiement de résultats (données, modèles, documentation, tutoriaux). Demostrateurs (e.g., A||go).
    - Tests, CI/CD, outils MLOps.
